﻿using System;
using System.Globalization;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
//using EFCachingProvider;
//using EFCachingProvider.Caching;
//using EFCachingProvider.Web;
using LowercaseRoutesMVC;
using MVCForum.Domain.Constants;
using MVCForum.Domain.Events;
using MVCForum.Domain.Interfaces.Services;
using MVCForum.Domain.Interfaces.UnitOfWork;
using MVCForum.Utilities;
using MVCForum.Website.Application;

namespace MVCForum.Website
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication
    {

        public IUnitOfWorkManager UnitOfWorkManager
        {
            get { return DependencyResolver.Current.GetService<IUnitOfWorkManager>(); }
        }

        public IBadgeService BadgeService
        {
            get { return DependencyResolver.Current.GetService<IBadgeService>(); }
        }

        public ISettingsService SettingsService
        {
            get { return DependencyResolver.Current.GetService<ISettingsService>(); }
        }

        public ILoggingService LoggingService
        {
            get { return DependencyResolver.Current.GetService<ILoggingService>(); }
        }

        public ILocalizationService LocalizationService
        {
            get { return DependencyResolver.Current.GetService<ILocalizationService>(); }
        }

        public ILuceneService LuceneService
        {
            get { return DependencyResolver.Current.GetService<ILuceneService>(); }
        }

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });

            routes.MapRouteLowercase(
                "categoryUrls", // Route name
                string.Concat(AppConstants.CategoryUrlIdentifier, "/{slug}"), // URL with parameters
                new { controller = "Category", action = "Show", slug = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRouteLowercase(
                "categoryRssUrls", // Route name
                string.Concat(AppConstants.CategoryUrlIdentifier, "/rss/{slug}"), // URL with parameters
                new { controller = "Category", action = "CategoryRss", slug = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRouteLowercase(
                "topicUrls", // Route name
                string.Concat(AppConstants.TopicUrlIdentifier, "/{slug}"), // URL with parameters
                new { controller = "Topic", action = "Show", slug = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRouteLowercase(
                "memberUrls", // Route name
                string.Concat(AppConstants.MemberUrlIdentifier, "/{slug}"), // URL with parameters
                new { controller = "Members", action = "GetByName", slug = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRouteLowercase(
                "tagUrls", // Route name
                string.Concat(AppConstants.TagsUrlIdentifier, "/{tag}"), // URL with parameters
                new { controller = "Topic", action = "TopicsByTag", tag = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRouteLowercase(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        protected void Application_Start()
        {
            // Register routes
            AreaRegistration.RegisterAllAreas();
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            // Store the value for use in the app
            Application["Version"] = AppHelpers.GetCurrentVersionNo();

            // If the same carry on as normal
            LoggingService.Initialise(ConfigUtils.GetAppSettingInt32("LogFileMaxSizeBytes", 10000));
            LoggingService.Error("START APP");

            // Set default theme
            var defaultTheme = "Metro";

            // Only load these IF the versions are the same
            if (AppHelpers.SameVersionNumbers())
            {
                // Get the theme from the database.
                defaultTheme = SettingsService.GetSettings().Theme;

                // Do the badge processing
                using (var unitOfWork = UnitOfWorkManager.NewUnitOfWork())
                {
                    try
                    {
                        BadgeService.SyncBadges();
                        unitOfWork.Commit();
                    }
                    catch (Exception ex)
                    {
                        LoggingService.Error(string.Format("Error processing badge classes: {0}", ex.Message));
                    }
                }

            }

            // Set the view engine
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new ForumViewEngine(defaultTheme));

            // Initialise the events
            EventManager.Instance.Initialize(LoggingService);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            //var app = (HttpApplication)sender;

            //var context = app.Context;

            //// Attempt to peform first request initialization

            //FirstRequestInitialization.Initialize(context);

            if (!AppHelpers.IsStaticResource(this.Request) && !AppHelpers.SameVersionNumbers() && !AppHelpers.InInstaller())
            {
                Response.Redirect(string.Concat("~", AppConstants.InstallerUrl));
            }
        }

        public class FirstRequestInitialization
        {
            private static bool _sInitializedAlready = false;

            private static readonly Object s_lock = new Object();

            // Initialize only on the first request

            private static string GetSiteUrl(HttpContext context)
            {

                string baseUrl = null;

                if (context != null)
                {

                    string port = context.Request.ServerVariables["SERVER_PORT"];

                    if (port == null || port.Equals("80") || port.Equals("443"))

                        port = String.Empty;

                    else

                        port = ":" + port;

                    string protocol = context.Request.ServerVariables["SERVER_PORT_SECURE"];

                    if (protocol == null || protocol.Equals("0"))

                        protocol = "http://";

                    else

                        protocol = "https://";

                    baseUrl = protocol + context.Request.ServerVariables["SERVER_NAME"] + port + context.Request.ApplicationPath;

                }
                return baseUrl;

            }

            public static void Initialize(HttpContext context)
            {

                if (_sInitializedAlready)
                {

                    return;

                }

                lock (s_lock)
                {

                    if (_sInitializedAlready)
                    {

                        return;

                    }



                    _sInitializedAlready = true;

                }

            }
        }

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {

            //It's important to check whether session object is ready
            if (!AppHelpers.InInstaller())
            {
                if (HttpContext.Current.Session != null)
                {
                    // Set the culture per request
                    var ci = new CultureInfo(LocalizationService.CurrentLanguage.LanguageCulture);
                    Thread.CurrentThread.CurrentUICulture = ci;
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
                }
            }

        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            //foreach (var item in HttpContext.Current.Items.Values)
            //{
            //    var disposableItem = item as IDisposable;

            //    if (disposableItem != null)
            //    {
            //        disposableItem.Dispose();
            //    }
            //}
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            LoggingService.Error(Server.GetLastError());
        }

    }
}